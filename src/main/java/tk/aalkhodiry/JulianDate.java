package tk.aalkhodiry;


/**
 * The JulianDate class implements the Julian Date system which indicates the <br />
 * number of days since noon on January 1, -4712, i.e., January 1, 4713 BC/BCE. <br />
 * It was proposed by Joseph Justus Scaliger (1540-1609) in 1583 (with this <br />
 * system named for his father, Julius Caesar Scaliger). Day One is a day when <br />
 * three calendrical cycles converge (the 28 year Julian Calendar, or Solar <br />
 * Number; the 19 year Golden Number cycle; and the 15 year ancient Roman tax <br />
 * cycle, called the Indiction). Scaliger picked January 1, 4712 BC on the <br />
 * Julian Calendar as Day One. The three cycles coincide every 7980 years. <br />
 * <br />
 * That the Julian Day begins at Noon reflects the practice of the Astronomical<br />
 * or Nautical Day before 1925. The Civil Day of the same calendar date began <br />
 * the Midnight before the Astronomical or Nautical Day. <br />
 * <br />
 * JulianDate makes use of the GregorianCalendar class which assumes a switch <br />
 * over date (from the Julian CALENDAR) of 15 Oct 1582.
 * 
 * @author Aalkhodiry
 * 
 */
public class JulianDate {

	/**
	 * The following array keeps track of the number of days so far in the year,
	 * assuming that the year start in March, though the array begins with the
	 * entry for January.<br />
	 * Jan Feb Mar Apr <br />
	 * May Jun Jul Aug <br />
	 * Sep Oct Nov Dec <br />
	 */
	private int monthTotals[] = { 306, 337, 0, 31, 61, 92, 122, 153, 184, 214,
			245, 275 };

	// Checks to make sure that the specified Solar/Gregorian date
	// is legitimate.
	private boolean legalDate(int year, int month, int day) {
		if (year == 1582 & month == 10 & day > 4 & day < 15)
			return false;
		else if (day <= 0 | day > 31)
			return false;
		else if ((month == 4 | month == 6 | month == 9 | month == 11)
				& day > 30)
			return false;
		else if (month == 2) {
			if (year > 1582) {
				// Check Feb for Gregorian years.
				if ((year % 4 == 0 & year % 100 != 0)
						| (year % 100 == 0 & year % 400 == 0))

					return day <= 29;
				else
					return day <= 28;
			} else if (year % 4 == 0)
				return day <= 29;
			else
				return day <= 28;
		} else
			return true;
	}

	/**
	 * The following is INTENDED to be a generalization of the following code.<br />
	 * <br />
	 * 
	 * if (year >= 1900) correction -= 13; <br />
	 * else if (year >= 1800) correction -= 12; <br />
	 * else if (year >= 1700) correction -= 11; <br />
	 * else if (year >= 1582) correction -= 10; <br />
	 * 
	 * The algorithm used for this conversion was taken from:
	 * 
	 * @see http://www.friesian.com/numbers.htm
	 */
	private int gregorianCorrection(int year, int month, int day) {

		int correction = 0;

		if (year > 1582
				| (year == 1582 & (month > 10 | (month == 10 & day > 4)))) {
			correction -= 10;

			int cyclesOf400 = (year / 100 - 16) / 4;

			correction -= cyclesOf400 * 3;

			int yearInCycle = (year / 100 - 16) % 4;

			// Adjust for dates still in the 1500's.
			if (yearInCycle < 0)
				yearInCycle = 0;

			if (yearInCycle >= 3)
				correction -= 3;
			else
				correction -= yearInCycle;
		}

		return correction;
	}

	/**
	 * Returns true iff the year is a Gregorian Leap Year.
	 * 
	 * @param year
	 * @return
	 */
	@SuppressWarnings("unused")
	private boolean leapYear(int year) {
		if (year % 4 == 0 && year % 100 != 0)
			return true;
		else if (year % 100 == 0 && year % 400 == 0)
			return true;
		else
			return false;
	}

	/**
	 * Converts Gregorian date to JulianDate day count
	 * 
	 * @param year
	 * @param month
	 * @param day
	 * @return
	 * @throws IllegalDateException
	 */
	public static long toJulian(int year, int month, int day)
			throws IllegalDateException {
		JulianDate jd = new JulianDate();
		/**
		 * Checks to make sure that the specified Solar/Gregorian date is
		 * legitimate.
		 */
		if (!jd.legalDate(year, month, day))
			throw new IllegalDateException("Date is not valid");

		if (month == 1 | month == 2)
			year--;

		long scaligerYear = year + 4712;

		long dayNumber = (scaligerYear / 4) * 1461 + (scaligerYear % 4) * 365;

		/**
		 * The first Scaliger year was a leap year, so if the date is after Feb
		 * 28 4713 BC, the above equation didn't include 29 Feb for that first
		 * year, and it must be included here.
		 */
		if (scaligerYear >= 0)
			dayNumber++;

		dayNumber += jd.monthTotals[month - 1] + day;

		dayNumber += 59 + jd.gregorianCorrection(year, month, day);

		// Remember to subtract 1 since 1 Jan 4713 BC isn't counted.
		return dayNumber - 1;
	}

	/**
	 * Converting from the Julian day number to the Gregorian date
	 * 
	 * @see http://www.hermetic.ch/cal_stud/jdn.htm
	 * @see http://quasar.as.utexas.edu/BillInfo/JulianDatesG.html
	 * @return
	 */
	public static HDate toGregorian(long jDayC) {
		long l, n, i, j;
		int day, month, year;
		l = (jDayC + 68569);
		n = (4 * l) / 146097;
		l = l - (146097 * n + 3) / 4;
		i = (4000 * (l + 1)) / 1461001;
		l = l - (1461 * i) / 4 + 31;
		j = (80 * l) / 2447;
		day = (int) (l - (2447 * j) / 80);
		l = j / 11;
		month = (int) (j + 2 - (12 * l));
		
		
		year = (int) (100 * (n - 49) + i + l);
		
		return new HDate(year, month, day);
	}
}