package tk.aalkhodiry;

public class IllegalDateException extends Exception {
	private static final long serialVersionUID = 1L;

	public IllegalDateException() {
		super();
	}
	
	public IllegalDateException(String msg) {
		super(msg);
	}
}
