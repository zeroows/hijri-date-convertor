package tk.aalkhodiry.test;

import java.text.ParseException;
import java.util.Date;

import tk.aalkhodiry.Hijri;
import tk.aalkhodiry.IllegalDateException;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			System.out.println(Hijri.toHijri(2014, 01, 23));

	        System.out.println(" - - - "); 
			System.out.println(Hijri.toHijri(new Date()));
		} catch (IllegalDateException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} 
	}
}
