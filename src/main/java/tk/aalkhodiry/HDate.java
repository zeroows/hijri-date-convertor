package tk.aalkhodiry;

/**
 * Date object and it's immutable
 * @author Aalkhodiry
 *
 */
public class HDate {
	private long year;
	private long month;
	private long day;
	
	
	/**
	 * 
	 * @param year
	 * @param month
	 * @param day
	 */
	public HDate(int year, int month, int day){
		this.year = year;
		this.month = month;
		this.day = day;
	}
	
	public long getYear() {
		return year;
	}
	public long getMonth() {
		return month;
	}
	public long getDay() {
		return day;
	}
	
	public String toString(){
		return String.format("%d-%d-%d", year, month, day);
	}
}
